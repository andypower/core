<?php

namespace AppBundle\Protocollo\Exception;

class ParentNotRegisteredException extends BaseException
{
    protected $message = 'Parent item is not registered';

}
