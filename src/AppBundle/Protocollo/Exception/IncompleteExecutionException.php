<?php

namespace AppBundle\Protocollo\Exception;

class IncompleteExecutionException extends BaseException
{
  protected $message = 'Incomplete execution';
}
