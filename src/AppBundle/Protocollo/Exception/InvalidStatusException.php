<?php

namespace AppBundle\Protocollo\Exception;

class InvalidStatusException extends BaseException
{
    protected $message = 'Status is invalid';

}
